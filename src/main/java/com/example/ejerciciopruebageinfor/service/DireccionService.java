package com.example.ejerciciopruebageinfor.service;

import com.example.ejerciciopruebageinfor.entity.Direccion;
import com.example.ejerciciopruebageinfor.repository.DireccionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DireccionService {
    @Autowired
    private DireccionRepo direccionRepo;

    public List<Direccion> getDirecciones(){
        return direccionRepo.findAll();
    }
}
