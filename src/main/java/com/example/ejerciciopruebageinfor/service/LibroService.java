package com.example.ejerciciopruebageinfor.service;

import com.example.ejerciciopruebageinfor.entity.Autor;
import com.example.ejerciciopruebageinfor.entity.Libro;
import com.example.ejerciciopruebageinfor.repository.AutorRepo;
import com.example.ejerciciopruebageinfor.repository.LibroRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroService {

    @Autowired
    private LibroRepo libroRepo;

    @Autowired
    private AutorRepo autorRepo;

    public void addNewLibro(Libro libro) {
        libroRepo.save(libro);
    }

    public List<Libro> getLibros(){
        return libroRepo.findAll();
    }

    public void deleteLibro(int id){
        libroRepo.deleteById(id);
    }

    public List<Libro> getLibrosByAutor(int id){
        Autor autor = autorRepo.getOne(id);
        return autor.getLibros();
    }
}
