package com.example.ejerciciopruebageinfor.service;

import com.example.ejerciciopruebageinfor.entity.Autor;
import com.example.ejerciciopruebageinfor.entity.Direccion;
import com.example.ejerciciopruebageinfor.repository.AutorRepo;
import com.example.ejerciciopruebageinfor.repository.DireccionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class AutorService {

    @Autowired
    private AutorRepo autorRepo;

    @Autowired
    private DireccionRepo direccionRepo;

    @PostConstruct
    public void init(){
        if (!autorRepo.existsById(1)){
            autorRepo.save(new Autor(1, "Anónimo", null));
        }
    }


    public void addNewAutor(Autor autor, Direccion direccion){
        direccionRepo.save(direccion);
        autorRepo.save(new Autor(autor.getIdAutor(), autor.getNombre(), direccion));
    }

    public List<Autor> getAutors(){
        return autorRepo.findAll();
    }

    public void deleteAutor(int id){
        autorRepo.deleteById(id);
    }
}
