package com.example.ejerciciopruebageinfor.controller;

import com.example.ejerciciopruebageinfor.entity.Autor;
import com.example.ejerciciopruebageinfor.entity.Direccion;
import com.example.ejerciciopruebageinfor.service.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AutorController {

    @Autowired
    private AutorService autorService;


    @GetMapping("/")
    public String inicio(){
        return "index";
    }

    @GetMapping("/nuevoautor")
    public String addAutor(Model m){
        m.addAttribute("autorForm",new Autor());
        m.addAttribute("direccionForm", new Direccion());
        return "addautor";
    }


    @PostMapping("/nuevoautor/submit")
    public String addSubmit(@ModelAttribute("autorForm") Autor autor, @ModelAttribute("direccionForm")Direccion direccion){
        autorService.addNewAutor(autor, direccion);
        return "index";
    }


    @GetMapping("/listaautores")
    public String listarAutores(Model m){
        m.addAttribute("listaAutores", autorService.getAutors());
        return "autorlist";
    }


//    @DeleteMapping("/listaautores/{id}")
//    public String deleteAutor(@PathVariable("id") int id){
//        autorService.deleteAutor(id);
//        return "redirect:/listaautores";
//    }

    @GetMapping("/tablaautor/eliminar")
    public String eliminarAutor(@RequestParam("id") int id){
        autorService.deleteAutor(id);
        return "redirect:/listaautores";
    }




}
