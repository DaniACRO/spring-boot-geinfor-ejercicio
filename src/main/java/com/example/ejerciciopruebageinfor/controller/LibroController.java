package com.example.ejerciciopruebageinfor.controller;

import com.example.ejerciciopruebageinfor.entity.Libro;
import com.example.ejerciciopruebageinfor.service.AutorService;
import com.example.ejerciciopruebageinfor.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LibroController {

    @Autowired
    private LibroService libroService;

    @Autowired
    private AutorService autorService;


    @GetMapping("/nuevolibro")
    public String addLibro(Model m){
        m.addAttribute("libroForm", new Libro());
        m.addAttribute("autores", autorService.getAutors());
        return "addlibro";
    }


    @PostMapping("/nuevolibro/submit")
    public String addLibroSubmit(@ModelAttribute("libroForm") Libro libro){
        System.out.println(libro);
        libroService.addNewLibro(libro);
        return "index";
    }

    @GetMapping("/listalibros")
    public String listarLibros(Model m){
        m.addAttribute("listaLibros", libroService.getLibros());
        return "librolist";
    }

    @GetMapping("/tablaautor/view/{id}")
    public String verLibrosAutor(@PathVariable("id") int id, Model m){
        m.addAttribute("listaLibros",libroService.getLibrosByAutor(id) );
        return "librolist";
    }


    @GetMapping("/tablalibro/eliminar")
    public String eliminarLibro(@RequestParam("id") int id){
        libroService.deleteLibro(id);
        return "redirect:/listalibros";
    }
}
