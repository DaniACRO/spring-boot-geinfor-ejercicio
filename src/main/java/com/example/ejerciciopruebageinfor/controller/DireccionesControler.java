package com.example.ejerciciopruebageinfor.controller;

import com.example.ejerciciopruebageinfor.service.DireccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DireccionesControler {

    @Autowired
    private DireccionService direccionService;


    @GetMapping("/direcciones")
    public String getDirecciones(Model m){
       m.addAttribute("listaDirecciones", direccionService.getDirecciones());
       return "direcciones";
    }
}
