package com.example.ejerciciopruebageinfor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciopruebageinforApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjerciciopruebageinforApplication.class, args);
    }

}
