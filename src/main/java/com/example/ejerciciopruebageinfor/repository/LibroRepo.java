package com.example.ejerciciopruebageinfor.repository;

import com.example.ejerciciopruebageinfor.entity.Libro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibroRepo extends JpaRepository<Libro, Integer> {
}
