package com.example.ejerciciopruebageinfor.repository;

import com.example.ejerciciopruebageinfor.entity.Autor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutorRepo extends JpaRepository<Autor, Integer> {
}
