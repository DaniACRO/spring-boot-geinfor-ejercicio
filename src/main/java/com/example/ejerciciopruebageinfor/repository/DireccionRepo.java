package com.example.ejerciciopruebageinfor.repository;

import com.example.ejerciciopruebageinfor.entity.Direccion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DireccionRepo extends JpaRepository<Direccion, Integer> {
}
